# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* base_automation_hr_contract
# 
# Translators:
# Jaroslav Bosansky <jaro.bosansky@ekoenergo.sk>, 2019
# Rastislav Brencic <rastislav.brencic@azet.sk>, 2020
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.4+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 12:55+0000\n"
"PO-Revision-Date: 2019-08-26 09:35+0000\n"
"Last-Translator: Rastislav Brencic <rastislav.brencic@azet.sk>, 2020\n"
"Language-Team: Slovak (https://www.transifex.com/odoo/teams/41243/sk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: sk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);\n"

#. module: base_automation_hr_contract
#: model:ir.model,name:base_automation_hr_contract.model_base_automation
msgid "Automated Action"
msgstr "Automatizovaná akcia"

#. module: base_automation_hr_contract
#: model:ir.model.fields,field_description:base_automation_hr_contract.field_base_automation__trg_date_resource_field_id
msgid "Use employee work schedule"
msgstr "Použiť rozvrh práce zamestnanca"

#. module: base_automation_hr_contract
#: model:ir.model.fields,help:base_automation_hr_contract.field_base_automation__trg_date_resource_field_id
msgid "Use the user's working schedule."
msgstr "Použiť pracovný rozvrh používateľa."
